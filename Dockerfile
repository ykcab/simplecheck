#The image we will use from Docker registry
FROM python:latest

## Add the file to our docker image
ADD . ./
COPY requirements.txt ./

#install all dependencies
RUN pip install --no-cache-dir -r requirements.txt

# since there are arguments needed, let use ENTRYPOINT to execute the program
ENTRYPOINT [ "python" , "./checker.py"]