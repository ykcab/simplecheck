#!/usr/bin/python3
__author__ = 'Alain'
__version__ = '0.2'
__email__ = 'asnip.mbuku@gmail.com'

# import needed modules and custom classes
import requests
from random import choice
import string
import hashlib
import time
import argparse
import random
import string
import sys
from classes.Colour import Colour
from classes.Argument import Arguments

# setting some global variables
args = Arguments.parser.parse_args()
rate = 1.3 #this rate limitation is enforced to not brack the program.
server = "https://api.pwnedpasswords.com/range" #https://api.pwnedpasswords.com/pwnedpassword/"
sslVerify = True
# fi you use proxiy, set this up
proxies = {
    #  'http': 'http://127.0.0.1:8080',    # Uncomment when needed
    #  'https': 'http://127.0.0.1:8080',   # Uncomment when needed
    }
length = int(args.length)

# main function to execute the program
def main():
  
    if length != None and 8 <= length <= 50:
        CheckString()
    else:
        print (Colour.FAILRED + "ERR! Check the Help menue (-h) for reference")

# define a function to generate random alphanumeric word with length determined with arg -l
def RandomKeyGenerator():
    charSet1 = string.ascii_letters + string.digits
    charSet2 = string.ascii_letters + string.digits + string.punctuation
    if len(sys.argv) < 4:
        return ''.join(choice(charSet1) for i in range(length))
    elif sys.argv[4] is not None:
        if sys.argv[4] != '1':
            print(Colour.FAILRED + "ERR! Check the Help menue (-h) for reference")
        else:
            return ''.join(choice(charSet2) for i in range(length))

#store the generated value into a global variable
x= RandomKeyGenerator()

# Hash x with sha1 crypto as per HBP API to match the data
def HashFunction():
    toHash = x.encode('utf-8')
    hashObject = hashlib.sha1(toHash)
    hashed = hashObject.hexdigest().upper()[:5]
    return hashed

# check the hashed value and retun the result
def CheckString():
    value = HashFunction()

    sleep = rate 
    check = requests.get(server + value + "?includeUnverified=true",
                proxies = proxies,
                verify = sslVerify)
    if (check.status_code) == 404:
        print (Colour.OKGREEN + "[i] Below passprahse has been verified and not found in known pastebin breaches.\nIt can be used as a secure password" 
                + Colour.ENDC)
        print  ("Copy 👉  " + Colour.CYAN + Colour.BOLD + x + Colour.ENDC)
        time.sleep(sleep)
        #return True
    elif check.status_code == 200:
        print (Colour.FAILRED + "[!] " + x + "  is recorded in known breached password!" + Colour.ENDC)
        time.sleep(sleep)
        return True
    elif check.status_code == 429:
        print (Colour.WARNING + "[!] Rate limit exceeded, server instructed us to retry after " + check.headers['Retry-After'] + " seconds" + Colour.ENDC)
        sleep = float(check.headers['Retry-After'])
        time.sleep(sleep)
        CheckString()
        return True
    else:
        print (Colour.WARNING + "[!] Something went wrong while checking " + Colour.ENDC)
        time.sleep(sleep)
        return True

if __name__ == "__main__":
    main()