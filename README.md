## What is this?
A simple script to generate a unique passphrase checked against known pwned passwords using the Have I been Pwned API. Proceed and use it with CAUTION!
## Getting Started
### Prerequisites
Python 3.4 or higher
Python `requests` module - can be dowloaded with `pip install requests` if you're not using a docker image.
### Usage
If you're running it in docker, pull the image from docker hub `docker pull ykcab/simplecheck`. The command is `docker run -t --rm ykcab/simplecheck -l [length]`.
Alternatively, you can simply clone the repository form GitHub to your local machine and run `python checker.py -l [length] -y 1`. The `-y 1` paramater is optional, but strongly recommanded to include it to generate a passphrase with special characters.
To access the helper menu, run `python cherker.py -h or --help` 
## Change Log
This is version 2. Added a docker image: **simplecheck**
As per HBP Policy guidelines, better to use hashed value (with SHA1) for checking the passphrase intead of plain text.
## Authors
* **Alain Mbuku** [ykcab](https://github.com/ykcab)
## Support
Pull requests are open.
## License
This project is licensed under the MIT License
## Acknowledgments
This tool was inspired from HBP porject by [Troy Hunt](https://troyhunt.com)
