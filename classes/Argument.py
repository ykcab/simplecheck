#!/usr/bin/python3

from random import choice
import string
import hashlib
import time
import argparse
import random
import string
import sys

class Arguments:
    parser = argparse.ArgumentParser(description="This tool generates a unique passphrase which has not yet been recorded in Pwned Passwords.\n Use it with CAUTION!")
    parser.add_argument("-l", dest="length", type= int,
                        help="an integer which determines the length of the passphrase and must be between 8 and 50 only!")
    parser.add_argument("-y", dest = "mode", type= int,
                        help="a bit to enamble special characters - this value is ALWAYS = 1")