#!/usr/bin/python3

class Colour:
    BLUE = '\033[94m'
    BOLD = '\033[1m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    ENDC = '\033[0m'
    FAILRED = '\033[91m'
    OKGREEN = '\033[92m'
    PURPLE = '\033[95m'
    UNDERLINE = '\033[4m'
    WARNING = '\033[93m'
    